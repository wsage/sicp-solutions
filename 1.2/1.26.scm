
(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp)
	 (remainder (* (expmod base (/ exp 2) m)
		       (expmod base (/ exp 2) m))
		    m))
	(else
	 (remainder (* base (expmod base (- exp 1) m))
		    m))))

Clearly, we now call expmod twice. At each computation the amount of steps divides and multiplies by 2.
The total amount of steps is proportional to exp.