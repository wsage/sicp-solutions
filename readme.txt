This is my solutions to the sicp course.

To load any file into the interpreter, run the following command:

(load "differentiation.scm")

If you run scheme on windows, open the right-click menu of the MIT-GNU Scheme shortcut and choose properties, next edit the Start in option to your likes and preferences.